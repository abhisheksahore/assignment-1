// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const inventory = require('./inventory.js');
const getYearsOfCarsBefore2000 = require('./problem5');
const getYearsOfCars = require('./problem4.js')

const yearsArray = getYearsOfCarsBefore2000(getYearsOfCars(inventory));

console.log(yearsArray.length);

