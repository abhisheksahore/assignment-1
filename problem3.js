// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortCarModels(inventory) {
    if (Array.isArray(inventory) && inventory.length > 0) {
        let sortedInventory = inventory.sort((a, b)=>{
            if(a.car_model.toUpperCase() > b.car_model.toUpperCase()) {
                return 1;
            } else {
                return -1;
            }
        })
        return sortedInventory;
    } else {
        return [];
    }    
}

module.exports = sortCarModels;