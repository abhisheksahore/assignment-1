// // ==== Problem #2 ====
// // The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 
// "Last car is a *car make goes here* *car model goes here*"


function getLastCarInInventory(inventory) {
    if (Array.isArray(inventory) && inventory.length > 0) {
        let max = -1;
        let ind = -1;
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i].id > max) {
                max = inventory[i].id;
                ind = i;
            }
        }
        if (max !== -1){
            return inventory[ind];
        } else {
            return [];
        }
    } else {
        return [];
    }
}

module.exports = getLastCarInInventory;
