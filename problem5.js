// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


function getYearsOfCarsBefore2000(inventory) {
    
    if (Array.isArray(inventory) && inventory.length > 0) {
        let yearsArray = [];
        
        for (let i = 0; i < inventory.length; i++) {
            if(inventory[i] < 2000){
                yearsArray.push(inventory[i]);
            }
        }
        return yearsArray;
    } else {
        return [];
    }
}

module.exports = getYearsOfCarsBefore2000;
