// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.


function getYearsOfCars(inventory) {
    
    if (Array.isArray(inventory) && inventory.length > 0) {
        let yearsArray = [];

        if (inventory[0].id === undefined){
            return [];
        }

        for (let i = 0; i < inventory.length; i++) {
            yearsArray.push(inventory[i].car_year);
        } 

        return yearsArray;
        
    } else {
        return [];
    }
}

module.exports = getYearsOfCars;
