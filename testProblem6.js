// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.



const inventory = require('./inventory');
const BMWAndAudi = require('./problem6');

// undefined, ['BMW', 'AUDI']
const preferredCarsList = BMWAndAudi(inventory, 'land rover') // we can pass second argument as car_maker name or an array of car_maker names.

if (Array.isArray(preferredCarsList) && preferredCarsList.length > 0) {
    console.log(JSON.stringify(preferredCarsList));
} else {
    console.log(preferredCarsList);

}
